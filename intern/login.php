<?php include('functions.php') ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
	<!-- Bootstrap core CSS -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/css/mdb.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Shrikhand" rel="stylesheet">
	<style media="screen">
		h1, h2, h3 {
			font-family: 'Shrikhand', cursive;
			text-transform: uppercase;
			text-align: center;
		}
		body, html {
			font-family: 'Montserrat', sans-serif;
			text-transform: uppercase;
		}
	</style>

</head>
<body>

	<div class="header">
		<h2>Login</h2>
	</div>

	<form method="post" action="login.php">

		<?php echo display_error(); ?>
		<div class="md-form">
			<input type="text" id="Username" name="username" class="form-control">
			<label for="Username">Username</label>
		</div>
		<div class="md-form">
			<input type="text" id="Password" name="password" class="form-control">
			<label for="Password">Password</label>
		</div>
		<div class="text-center mt-4">
				<button class="btn btn-outline-secondary" name="login_btn" type="submit">Send<i class="fas fa-paper-plane ml-2"></i></button>
		</div>

	</form>
    <div class="container-fluid">
        <center>&copy; <a href="https://fq-programming.xyz">FQ-Programming</a></center>
    </div>
	<!-- JQuery -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/js/mdb.min.js"></script>

</body>
</html>
